/* shARe, an XR project
 * Copyright (C) 2022 I2M
 *
 * SphereGenerator.cs - An Unity Package for making interactive 3D video player spheres
 * Copyright (C) 2022 Lucas Marquès <lucas.marques@u-bordeaux.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace Video360Player.Scripts
{
    /**
     * Class containing all the data for representing a 360VideoPlayer
     */
    class Player360
    {
        public Vector3 Position { get; }
        public VideoClip Video { get; }
        public GameObject SphereGO { get; set; }
        public GameObject VideoPlayerGO { get; set; }
        public RenderTexture Texture { get; set; }
        public Player360(Vector3 position, VideoClip video)
        {
            Position = position;
            Video = video;
        }
    }
    public class SphereGenerator : MonoBehaviour
    {
        /* === keep positions and videos in the same order */
        [SerializeField] private GameObject head = null;
        [SerializeField] private bool playOnAwake = true;
        [SerializeField] private int sphereSize = 3 ;
        [SerializeField] private List<Vector3> spherePositions;
        [SerializeField] private List<VideoClip> sphereVideos;

        private List<Player360> spheres;

        private void Awake()
        {
            spheres = new List<Player360>();
            if (spherePositions.Count != sphereVideos.Count)
            {
                Debug.LogError("Difference between the amount of given sphere positions and the amount " +
                               "of available video clips");
                throw new Exception(
                    "Difference between the amount of given sphere positions and the amount of available" +
                    " video clips");
            } 
            for (var i = 0; i < spherePositions.Count; i++)
            {
                spheres.Add(new Player360(spherePositions[i], sphereVideos[i]));
            }
            if (head != null)
            {
                var headgoprefab = Resources.Load<GameObject>("GOFollowCamera");
                var headgo = Instantiate(headgoprefab);
                headgo.GetComponent<GOFollowCamera>().camera = head;
            }
            GenerateSpheres();
        }

        private void GenerateSpheres()
        {
            for (var i = 0; i < spheres.Count; i++)
            {
                var sphere = spheres[i];
                /* We load our custom material with emission enabled by default on its shader */
                Material currMaterial = Resources.Load<Material>("Material/360SphereViewer");
                /* We generate a fresh custom sized RenderTexture that will be applied as Albedo and Emission textures */
                RenderTexture currRenderTexture = new RenderTexture((int) sphere.Video.width, (int) sphere.Video.height,
                    (int) RenderTextureFormat.ARGB32);
                /* Generating the Sphere GameObject */
                GameObject currSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);  // Generating the primitive
                currSphere.transform.position = sphere.Position;  // Setting its position in space
                currSphere.transform.localScale = Vector3.one * sphereSize;  // Rescaling it to the desired size
                currSphere.GetComponent<Renderer>().material = currMaterial;  // Attaching the Material created above
                currSphere.GetComponent<SphereCollider>().isTrigger = true;  // Enabling Trigger mode on Sphere for the Viewer Script
                currSphere.AddComponent(Type.GetType("Video360Player.Scripts.Sphere360Viewer"));  // Attaching it the Viewer Script
                currMaterial = currSphere.GetComponent<Renderer>().material;  // Direct access to the Sphere Material as it will be a copy of the Material created before that can be edited without side effect
                
                /* Réglages du Shader */
                currMaterial.globalIlluminationFlags = MaterialGlobalIlluminationFlags.BakedEmissive;
                currMaterial.EnableKeyword("_Emission");
                currMaterial.SetTexture("_MainTex", currRenderTexture);
                currMaterial.SetTexture("_EmissionMap", currRenderTexture);
                currMaterial.SetColor("_EmissionColor", Color.white);
                
                /* Réglages du Player */
                GameObject currSphereVideoPlayer = new GameObject("VideoPlayer#" + i + "-" + sphere.Video.name);
                var videoPlayerComponent = currSphereVideoPlayer.AddComponent<VideoPlayer>();
                videoPlayerComponent.isLooping = true;
                videoPlayerComponent.audioOutputMode = VideoAudioOutputMode.AudioSource;
                videoPlayerComponent.renderMode = VideoRenderMode.RenderTexture;
                videoPlayerComponent.clip = sphere.Video;
                videoPlayerComponent.targetTexture = currRenderTexture;
                currSphere.GetComponent<Sphere360Viewer>().assignedPlayer = videoPlayerComponent;

                var audioSourceComponent = currSphere.AddComponent<AudioSource>();
                audioSourceComponent.volume = 0f;
                currSphere.GetComponent<Sphere360Viewer>().assignedSource = audioSourceComponent;
                
                sphere.SphereGO = currSphere;
                sphere.VideoPlayerGO = currSphereVideoPlayer;
                sphere.Texture = currRenderTexture;
                videoPlayerComponent.playOnAwake = playOnAwake;
            }
        }
    }
}