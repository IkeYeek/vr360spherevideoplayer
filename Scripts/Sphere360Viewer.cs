/* shARe, an XR project
 * Copyright (C) 2022 I2M
 *
 * Sphere360ViewerScript.cs - An Unity Package for making interactive 3D video player spheres
 * Copyright (C) 2022 Lucas Marquès <lucas.marques@u-bordeaux.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using UnityEngine;
using UnityEngine.Video;

namespace Video360Player.Scripts
{
    public class Sphere360Viewer : MonoBehaviour
    {
        // Vector parametrizing the recaling of the sphere. Use 0 for no rescale. Rescaling sometimes allow for better render
        private readonly Vector3 scaleVector = new Vector3(0, 0, 0);
        // Garder l'état courant des normales et des faces de la sphère. 
        private bool isInverted;
        // Normales par défaut de la sphère
        private Vector3[] normals;
        // Normales inversées de la sphère
        private Vector3[] invertedNormals;
        // Mesh triangles
        private int[] triangles;
        // Inverted (replaced) mesh triangles
        private int[] invertedTriangles;
        public VideoPlayer assignedPlayer;
        public AudioSource assignedSource;

        /**
     * Lorsque l'objet est chargé dans la scène, on sauvegarde l'état de ses normales (pas inversées)
     * On précalcule également les normales et les triangles inversés. Dès lors que l'on rentre et sors d'une bulle,
     * il est intéressant de précalculer ces valeurs. 
    */
        private void Awake()
        {
            isInverted = false;
            ComputeNormals();
            
        }
    
        /* === Les deux triggers qui chargent les inverses des normales / triangles courants. === */
        void OnTriggerEnter(Collider other) 
        {
            Invert();
        }

        private void OnTriggerExit(Collider other)
        {
            Invert();
        }
        /**
     * Applique les normales et les triangles inversés au mesh courant. Le faire sur mesh et non sharedMesh pour
     * éviter de persister une inversion.
     * Comme on peut mettre la bulle à l'échelle, pour l'agrandir quand on rentre dedans et par conséquent changer
     * ses caractéristiques dans le moteur de jeu, on le déplace de la moitié de l'agrandissement vers le haut pour que
     * la bulle reste au dessus du sol
     */
        private void Invert()
        {
            var localTransform = transform;
        
            if (isInverted)
            {
                gameObject.GetComponent<MeshFilter>().mesh.normals = normals; 
                gameObject.GetComponent<MeshFilter>().mesh.triangles= triangles;
                localTransform.localScale -= scaleVector;
                localTransform.position -= new Vector3(0, scaleVector.y/2, 0);
                localTransform.rotation *= Quaternion.Euler(0, 180, 0);
            }
            else
            {
                gameObject.GetComponent<MeshFilter>().mesh.normals = invertedNormals;
                gameObject.GetComponent<MeshFilter>().mesh.triangles= invertedTriangles;
                localTransform.localScale += scaleVector;
                localTransform.position += new Vector3(0, scaleVector.y/2, 0);
                localTransform.rotation *= Quaternion.Euler(0, -180, 0);
            }

            isInverted = !isInverted;
        }
    
        /**
     * Précalcule les normales et les triangles de la sphère inversée
     */
        void ComputeNormals()
        {
            invertedNormals = gameObject.GetComponent<MeshFilter>().sharedMesh.normals;
            normals = new Vector3[invertedNormals.Length];
            for (var normalIndex = 0; normalIndex < invertedNormals.Length; normalIndex += 1)
            {
                normals[normalIndex] = invertedNormals[normalIndex];
            }
            for(int i = 0; i < invertedNormals.Length; i++)
            {
                invertedNormals[i] = -invertedNormals[i];
            }
        
            invertedTriangles = gameObject.GetComponent<MeshFilter>().sharedMesh.triangles;
            triangles = new int[invertedTriangles.Length];
            for (var triangleIndex = 0; triangleIndex < invertedTriangles.Length; triangleIndex += 1)
            {
                triangles[triangleIndex] = invertedTriangles[triangleIndex];
            }
            for (int i = 0; i < invertedTriangles.Length; i+=3)
            {
                (invertedTriangles[i], invertedTriangles[i + 2]) = (invertedTriangles[i + 2], invertedTriangles[i]);
            }           

        }
    
        void OnApplicationQuit()
        {
            if (isInverted)
                Invert();
        }
    }
}