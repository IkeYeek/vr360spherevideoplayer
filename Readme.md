﻿## Video360Player
`Lucas <Ike > Marquès - I2M Bordeaux`

A simple Unity asset for generating and customizing VR-Ready 360 video spheres in Unity.

This asset provides almost out-of-the-box usage for simple needs, can be slightly configured and especially, can be extended easily

All the samples provides from youtube videos
- https://www.youtube.com/watch?v=kLVajtkrG_4
- https://www.youtube.com/watch?v=UUzQcPuK8uk
- https://www.youtube.com/watch?v=6kK8ZrPE0dk
- https://www.youtube.com/watch?v=-LdqkOE_leo
### Usage

To get started, create a new scene in Unity or open the one you want to add the Spheres to.

Create then an empty GameObject in your scene, and add the SphereGenerator script to it. That's almost it !

You have several parameters you have to / can work with depending on what you are trying to do.

- Head, A reference to the player's head if he doesn't have a RigidBody (leave null if it already has). Useful for VR cameras for example
- Play On Awake VideoPlayer / AudioSource parameter
- Sphere Positions, A List of Vector3 containing the coordinates of each sphere
- Sphere Video, A list of VideoClip containing the desired videos in the same order than the positions

####/!\ Keep the exact same number of positions and videos /!\

### Tips to extend the usage

In order to make possible adding new stuff easily, you have a `private List<Player360> spheres;` that contains all the data you need on the spheres in order to be able to customize their behaviors

### To Do

- Add an option to start / stop sound by entering a sphere (muted by default)
- Add an unmute by default option
- Add an option to restart the video when entering the sphere
- Add an option to pause the video on the first frame (default play on awake will not display any texture) - trick because it seem to pause all the players when the first one gets ready, not displaying most of the others ones
- Add the resize option